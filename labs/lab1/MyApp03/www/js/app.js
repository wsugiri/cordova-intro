(function () {
	// console.log('aa');
	var list = JSON.parse(localStorage.getItem('list'));
	var html = '';
	list.forEach(function (item) {
		html += '<div>' + item.nama + '</div>';
	});
	$('[data-page=list] .content').html(html);

	$('[data-page=list2]').addClass('active');

	var tmpl = [
		'{{#each this}}',
		'<div>',
		'Nama: {{nama}}<br/> Alamat: {{alamat}}<br/> Umur: {{umur}}',
		'</div>',
		'<br/>',
		'{{/each}}'
	];

	var html = Handlebars.compile(tmpl.join(''))(list);
	$('[data-page=list2] .content').html(html);

	$('a').on('click', function (e) {
		e.preventDefault();
		var link = $(this).attr('href');

		$('[data-page].active').removeClass('active');
		$('[data-page=' + link + ']').addClass('active');
	});
}());
