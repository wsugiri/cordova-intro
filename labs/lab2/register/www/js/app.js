var app = new Framework7({
	modalTitle: 'Mobile Demo',
	pushState: true,
	onAjaxStart: function (xhr) { app.showIndicator(); },
	onAjaxComplete: function (xhr) { app.hideIndicator(); },
});
// app.name = 'mdemo';
app.helper = {
	serialize: function () {
		var list = $$('.page.page-on-center [data-field]');
		var obje = {};

		for (var i = 0; i < list.length; i++) {
			var item = $$(list[i]);
			obje[item.data('field')] = item.val();
		}

		return obje;
	},
	populate: function (obje) {
		for (var key in obje) {
			$$('.page.page-on-center [data-field=' + key + ']').val(obje[key]);
		}
	}
};
app.lsname = 'mdemo';
app.lsdata = {
	save: function (name, row, key) {
		var list = app.lsdata.get(name);
		if (list == undefined) {
			list = [];
			list.push(row);
		} else {
			if (key && row[key]) {
				var _key = {};
				_key[key] = row[key];

				var _row = _.find(list, _key);
				if (_row) {
					for (var field in row) {
						_row[field] = row[field];
					}
				} else {
					list.push(row);
				}
			} else {
				list.push(row);
			}
		}

		localStorage.setItem(app.lsname + '.' + name, JSON.stringify(list));
	},
	get: function (name, filter) {
		var list = JSON.parse(localStorage.getItem(app.lsname + '.' + name));
		if (filter) {
			return _.find(list, filter);
		} else {
			return list;
		}
	}
};

var $$ = Dom7;
var mainView = app.addView('.view-main', {});

$$(document).on('pageInit', function (e) {
	var page = e.detail.page;
	require(['views/' + page.name], function (option) {
		if (option && option.init) {
			option.init();
		}
	});
});
