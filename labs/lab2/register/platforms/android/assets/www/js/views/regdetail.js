define({
	init: function () {
		var filter = $$.parseUrlQuery(window.location.href);
		var row = app.lsdata.get('register', filter);

		setTimeout(function () {
			app.helper.populate(row);
			$$('.page.page-on-center img').attr({src: row.imageURI || 'img/noimage.png'});
			$$('.page.page-on-center [data-action=save]').on('click', function () {
				var person = app.helper.serialize();
				app.lsdata.save('register', person, 'email');
				mainView.router.reloadPage('views/reg_list.html');
			});

			$$('.page.page-on-center [data-action=takepict]').on('click', function () {
				console.log('take pict');
				if (navigator && navigator.camera && navigator.camera.getPicture) {
					// navigator.camera.getPicture(function () {}, function () {}, {});

					navigator.camera.getPicture(function (imageURI) {
						var image = $$('.page.page-on-center img');
						image.attr({
							src: imageURI
						});
						var person = app.helper.serialize();
						person.imageURI = imageURI;
						app.lsdata.save('register', person, 'email');
					}, function (message) {
						alert('Failed because: ' + message);
					});
				}
			});
		}, 400);
	}
});
