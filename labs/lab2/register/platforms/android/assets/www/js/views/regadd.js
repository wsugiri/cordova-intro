define({
	init: function () {
		setTimeout(function () {
			$$('.page.page-on-center [data-action=save]').on('click', function () {
				var person = app.helper.serialize();

				if (person.name.length < 3) {
					app.alert('Check isian data name');
					return;
				}

				if (person.email.length < 3) {
					app.alert('Check isian data email');
					return;
				}

				if (app.lsdata.get('register', { name: person.name })) {
					app.alert('nama sudah terdaftar');
					return;
				}

				if (app.lsdata.get('register', { email: person.email })) {
					app.alert('email sudah terdaftar');
					return;
				}

				app.lsdata.save('register', person, 'email');
				mainView.router.reloadPage('views/reg_list.html');
			});
		}, 400);
	}
});
