define({
	init: function () {
		setTimeout(function () {
			var html = '';
			var tmpl = [
                '<div class="list-block">',
                '<ul>',
				'{{#each this}}',
				'<li>',
                '<a href="views/reg_detail.html?email={{email}}" class="item-link item-content">',
                '<div class="item-inner">',
				' <div class="item-title-row">',
                '  <div class="item-title">{{name}}</div>',
				'  <div class="item-text">Phone : {{phone||"-"}}</div>',
				' </div>',
				' <div class="item-text">{{email}}</div>',
				'</div>',
                '</a>',
                '</li>',
				'{{/each}}',
                '</ul>',
                '<div>',
			];

			console.log('list menu');

			var list = app.lsdata.get('register');
			html = Template7.compile(tmpl.join(''))(list);
            $$('.page.page-on-center .page-content').html(html);
		}, 400);
	}
});
